package ExifStat;
use strict;
use warnings;
use Image::ExifTool qw(:Public);
use File::Spec;
use List::Util qw(min max);

sub processFile {
  my ($Stat,$path)=@_;
  #print $path."\n";
  my %info=%{ImageInfo($path)};
  my $focalStr=$info{'FocalLengthIn35mmFormat'};
  my $focalNum=0;
  if(defined($focalStr)) {
    $focalStr =~ /^(\w*)/;
    $focalNum=int($1)&~0x3; # step of four
  }
  my $lensStr=$info{'LensID'};
  $lensStr=$info{'LensModel'} if(!defined($lensStr));
  $lensStr=$info{'Model'} if(!defined($lensStr));
  $lensStr="unknown" if(!defined($lensStr) || !length($lensStr));

  $$Stat{$lensStr}{$focalNum}++;
}

sub processPath {
  my ($Stat,$path)=@_;
  #print $path."\n";
  if( -d $path) {
    opendir(my $dh, $path) || return;
    my @files=File::Spec->no_upwards(readdir($dh));
    closedir($dh);
    for my $file (@files) {
      processPath($Stat,File::Spec->catfile($path,$file));
    }
  } elsif( -r $path && $path =~ /\.jpg/i) {
    processFile($Stat, $path);
  }
}

sub focalMinMax {
  my $Stat=shift;
  my @mins;
  my @maxes;
  for my $lens (keys %{$Stat}) {
    my @focals=keys %{$$Stat{$lens}};
    push(@mins,min(@focals));
    push(@maxes,max(@focals));
  }
  return (min(@mins), max(@maxes));
}

1;
