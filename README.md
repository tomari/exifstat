# ExifStat

GNUPLOT preprocessor for statistical information of lens and 35-mm Format
equivalent focal length. What's that? See here:

<img src="exifstat_20160126-192900.png" alt="screenshot">

## Requirements

* Image::ExifTool (libimage-exiftool-perl in debian)
* perl (tested on 5.20.2)

## Usage

	exifstat-csv.pl ${HOME}/photo

This recursively searches for JPEG files in the directory.
The script will output two files: a CSV file, and a GNUPLOT script.

Run

	gnuplot exifstat_20160126-192900.gpl

to obtain exifstat_20160126-192900.png

## License

Public Domain
