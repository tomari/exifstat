#!/usr/bin/env perl
use strict;
use warnings;
use Time::localtime;
use FindBin;
use lib $FindBin::RealBin;
use ExifStat;
my $usage = << 'EOF';
exifstat-csv: Export EXIF statistics result to CSV format
usage:  exifstat-csv [-ng] PATH PATH PATH ...
    recursively scans PATH for JPEG images, and outputs
    a matrix showing focal length in 35 mm format and lens used.

Option:
  -ng : Do not output gnuplot script
EOF

sub exportStat {
  my ($Stat, $basename, $gnuplot)=@_;
  my $csvpath=$basename.".csv";
  die 'output file '.$csvpath.' already exists. exiting.' if( -e $csvpath);
  my $sep=$gnuplot?"\t":",\t";
  open(my $csv, '>', $csvpath) or die 'cannot open '.$csvpath;
  print $csv 'Lens'.$sep;

  my @focals;
  my ($fmin, $fmax)=ExifStat::focalMinMax($Stat);
  for(my $f=$fmin; $f<=$fmax; $f+=4) {
    push(@focals,$f);
    print $csv $f.$sep;
  }
  print $csv "\n";

  my @lenses=sort keys %{$Stat};
  for my $lens (@lenses) {
    my $lens_label=$lens;
    $lens_label =~ s/\"//g;
    print $csv '"'.$lens_label.'"'.$sep;
    for my $focal (@focals) {
      my $count=0;
      my $v=$$Stat{$lens}{$focal};
      $count=$v if(defined($v));
      print $csv $count.$sep;
    }
    print $csv "\n";
  }
  close $csv;

  if($gnuplot) {
    my $gplpath=$basename.".gpl";
    die 'output file '.$gplpath.' already exists. exiting.' if( -e $gplpath);
    open(my $gpl, '>', $gplpath) or die 'cannot open '.$gplpath;
    print $gpl "set term png size 1600,480\n";
    print $gpl 'set output "'.$basename.'.png"'."\n";
    print $gpl <<'EOL';
set style histogram columnstacked
set style fill solid border lc rgb "black"
set xtics rotate by -90
set key outside
set xlabel '35 mm Format equiv. focal length'
set ylabel 'number of images'
EOL
    print $gpl 'plot ';
    my $i=2;
    my $key=':key(1)';
    for my $focal (@focals) {
      print $gpl '"'.$csvpath.'" using '.$i.$key.' with histogram title columnhead,\\'."\n";
      $key='';
      $i++;
    }
    print $gpl "\n\n";
    close $gpl;
  }
}

sub makeFilename {
  my $now=localtime();
  my $basename=sprintf('exifstat_%d%02d%02d-%02d%02d%02d',
	1900+($now->year()),1+($now->mon()),$now->mday(),
	$now->hour(),$now->min(),$now->sec());
  return $basename;
}

my $gnuplot=1;
if($ARGV[0] eq "-ng") {
  $gnuplot=0;
  shift @ARGV;
}

if(defined($ARGV[0]) && -d $ARGV[0]) {
  my $basename=makeFilename();
  my %Stat;
  while(my $path=shift @ARGV) {
    ExifStat::processPath(\%Stat, $path);
  }
  if(keys %Stat>0) {
    exportStat(\%Stat, $basename, $gnuplot);
  }
} else {
  print $usage;
}
